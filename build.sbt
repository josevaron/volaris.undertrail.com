import _root_.ohnosequences.sbt.SbtS3Resolver.autoImport.{s3, _}
import com.amazonaws.auth.AWSCredentials
import sbt.Keys._

name := """interjet"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.8"

resolvers ++= Seq[Resolver](
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases",
  s3resolver.value("Undertrail resolver", s3("repo.undertrail.com")).withIvyPatterns
)

libraryDependencies ++= Seq(
  "org.scalaj" %% "scalaj-http" % "1.1.6",
  "com.undertrail" %% "base-scraper" % "1.1.0-SNAPSHOT",
  jdbc,
  cache,
  ws,
  specs2 % Test,
  "org.jsoup" % "jsoup" % "1.8.3"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator


fork in run := true

s3credentials := new AWSCredentialsProvider() {
  override def refresh(): Unit = {}

  override def getCredentials: AWSCredentials = new AWSCredentials {
    override def getAWSAccessKeyId: String = "AKIAJ37N3P5AJZLIIHNA"

    override def getAWSSecretKey: String = "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"
  }
}
