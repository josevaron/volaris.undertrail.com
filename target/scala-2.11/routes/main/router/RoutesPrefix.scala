
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/josevaron/volaris.undertrail.com/conf/routes
// @DATE:Fri Sep 09 10:06:21 COT 2016


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
