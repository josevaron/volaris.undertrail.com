
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/josevaron/volaris.undertrail.com/conf/routes
// @DATE:Fri Sep 09 10:06:21 COT 2016

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Application_1: controllers.Application,
  // @LINE:7
  ScraperCtrl_2: com.undertrail.scrapers.controllers.ScraperCtrl,
  // @LINE:12
  Assets_0: controllers.Assets,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Application_1: controllers.Application,
    // @LINE:7
    ScraperCtrl_2: com.undertrail.scrapers.controllers.ScraperCtrl,
    // @LINE:12
    Assets_0: controllers.Assets
  ) = this(errorHandler, Application_1, ScraperCtrl_2, Assets_0, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Application_1, ScraperCtrl_2, Assets_0, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Application.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/trips""", """com.undertrail.scrapers.controllers.ScraperCtrl.getTrips(origin:String, destination:String, departureDate:String, returnDate:Option[String], adults:Int ?= 1, children:Int ?= 0, infants:Int ?= 0, cached:Boolean ?= true)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/trips/get""", """com.undertrail.scrapers.controllers.ScraperCtrl.getTrip()"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/booking""", """com.undertrail.scrapers.controllers.ScraperCtrl.bookTrip()"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """assets/$file<.+>""", """controllers.Assets.versioned(path:String = "/public", file:Asset)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Application_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Application_index0_invoker = createInvoker(
    Application_1.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Application",
      "index",
      Nil,
      "GET",
      """ Home page""",
      this.prefix + """"""
    )
  )

  // @LINE:7
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_getTrips1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/trips")))
  )
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_getTrips1_invoker = createInvoker(
    ScraperCtrl_2.getTrips(fakeValue[String], fakeValue[String], fakeValue[String], fakeValue[Option[String]], fakeValue[Int], fakeValue[Int], fakeValue[Int], fakeValue[Boolean]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "com.undertrail.scrapers.controllers.ScraperCtrl",
      "getTrips",
      Seq(classOf[String], classOf[String], classOf[String], classOf[Option[String]], classOf[Int], classOf[Int], classOf[Int], classOf[Boolean]),
      "GET",
      """""",
      this.prefix + """api/trips"""
    )
  )

  // @LINE:8
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_getTrip2_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/trips/get")))
  )
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_getTrip2_invoker = createInvoker(
    ScraperCtrl_2.getTrip(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "com.undertrail.scrapers.controllers.ScraperCtrl",
      "getTrip",
      Nil,
      "POST",
      """""",
      this.prefix + """api/trips/get"""
    )
  )

  // @LINE:9
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_bookTrip3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/booking")))
  )
  private[this] lazy val com_undertrail_scrapers_controllers_ScraperCtrl_bookTrip3_invoker = createInvoker(
    ScraperCtrl_2.bookTrip(),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "com.undertrail.scrapers.controllers.ScraperCtrl",
      "bookTrip",
      Nil,
      "POST",
      """""",
      this.prefix + """api/booking"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_Assets_versioned4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("assets/"), DynamicPart("file", """.+""",false)))
  )
  private[this] lazy val controllers_Assets_versioned4_invoker = createInvoker(
    Assets_0.versioned(fakeValue[String], fakeValue[Asset]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Assets",
      "versioned",
      Seq(classOf[String], classOf[Asset]),
      "GET",
      """ Map static resources from the /public folder to the /assets URL path""",
      this.prefix + """assets/$file<.+>"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Application_index0_route(params) =>
      call { 
        controllers_Application_index0_invoker.call(Application_1.index)
      }
  
    // @LINE:7
    case com_undertrail_scrapers_controllers_ScraperCtrl_getTrips1_route(params) =>
      call(params.fromQuery[String]("origin", None), params.fromQuery[String]("destination", None), params.fromQuery[String]("departureDate", None), params.fromQuery[Option[String]]("returnDate", None), params.fromQuery[Int]("adults", Some(1)), params.fromQuery[Int]("children", Some(0)), params.fromQuery[Int]("infants", Some(0)), params.fromQuery[Boolean]("cached", Some(true))) { (origin, destination, departureDate, returnDate, adults, children, infants, cached) =>
        com_undertrail_scrapers_controllers_ScraperCtrl_getTrips1_invoker.call(ScraperCtrl_2.getTrips(origin, destination, departureDate, returnDate, adults, children, infants, cached))
      }
  
    // @LINE:8
    case com_undertrail_scrapers_controllers_ScraperCtrl_getTrip2_route(params) =>
      call { 
        com_undertrail_scrapers_controllers_ScraperCtrl_getTrip2_invoker.call(ScraperCtrl_2.getTrip())
      }
  
    // @LINE:9
    case com_undertrail_scrapers_controllers_ScraperCtrl_bookTrip3_route(params) =>
      call { 
        com_undertrail_scrapers_controllers_ScraperCtrl_bookTrip3_invoker.call(ScraperCtrl_2.bookTrip())
      }
  
    // @LINE:12
    case controllers_Assets_versioned4_route(params) =>
      call(Param[String]("path", Right("/public")), params.fromPath[Asset]("file", None)) { (path, file) =>
        controllers_Assets_versioned4_invoker.call(Assets_0.versioned(path, file))
      }
  }
}