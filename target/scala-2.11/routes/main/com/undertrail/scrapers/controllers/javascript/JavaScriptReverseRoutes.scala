
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/josevaron/volaris.undertrail.com/conf/routes
// @DATE:Fri Sep 09 10:06:21 COT 2016

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:7
package com.undertrail.scrapers.controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:7
  class ReverseScraperCtrl(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def getTrip: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "com.undertrail.scrapers.controllers.ScraperCtrl.getTrip",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/trips/get"})
        }
      """
    )
  
    // @LINE:9
    def bookTrip: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "com.undertrail.scrapers.controllers.ScraperCtrl.bookTrip",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/booking"})
        }
      """
    )
  
    // @LINE:7
    def getTrips: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "com.undertrail.scrapers.controllers.ScraperCtrl.getTrips",
      """
        function(origin,destination,departureDate,returnDate,adults,children,infants,cached) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/trips" + _qS([(""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("origin", origin), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("destination", destination), (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("departureDate", departureDate), (""" + implicitly[QueryStringBindable[Option[String]]].javascriptUnbind + """)("returnDate", returnDate), (adults == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("adults", adults)), (children == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("children", children)), (infants == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("infants", infants)), (cached == null ? null : (""" + implicitly[QueryStringBindable[Boolean]].javascriptUnbind + """)("cached", cached))])})
        }
      """
    )
  
  }


}