
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/josevaron/volaris.undertrail.com/conf/routes
// @DATE:Fri Sep 09 10:06:21 COT 2016

package com.undertrail.scrapers.controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final com.undertrail.scrapers.controllers.ReverseScraperCtrl ScraperCtrl = new com.undertrail.scrapers.controllers.ReverseScraperCtrl(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final com.undertrail.scrapers.controllers.javascript.ReverseScraperCtrl ScraperCtrl = new com.undertrail.scrapers.controllers.javascript.ReverseScraperCtrl(RoutesPrefix.byNamePrefix());
  }

}
