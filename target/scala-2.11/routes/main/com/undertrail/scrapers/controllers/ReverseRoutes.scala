
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/josevaron/volaris.undertrail.com/conf/routes
// @DATE:Fri Sep 09 10:06:21 COT 2016

import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:7
package com.undertrail.scrapers.controllers {

  // @LINE:7
  class ReverseScraperCtrl(_prefix: => String) {
    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:8
    def getTrip(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "api/trips/get")
    }
  
    // @LINE:9
    def bookTrip(): Call = {
      import ReverseRouteContext.empty
      Call("POST", _prefix + { _defaultPrefix } + "api/booking")
    }
  
    // @LINE:7
    def getTrips(origin:String, destination:String, departureDate:String, returnDate:Option[String], adults:Int = 1, children:Int = 0, infants:Int = 0, cached:Boolean = true): Call = {
      import ReverseRouteContext.empty
      Call("GET", _prefix + { _defaultPrefix } + "api/trips" + queryString(List(Some(implicitly[QueryStringBindable[String]].unbind("origin", origin)), Some(implicitly[QueryStringBindable[String]].unbind("destination", destination)), Some(implicitly[QueryStringBindable[String]].unbind("departureDate", departureDate)), Some(implicitly[QueryStringBindable[Option[String]]].unbind("returnDate", returnDate)), if(adults == 1) None else Some(implicitly[QueryStringBindable[Int]].unbind("adults", adults)), if(children == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("children", children)), if(infants == 0) None else Some(implicitly[QueryStringBindable[Int]].unbind("infants", infants)), if(cached == true) None else Some(implicitly[QueryStringBindable[Boolean]].unbind("cached", cached)))))
    }
  
  }


}