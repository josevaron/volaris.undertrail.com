package controllers

import play.api.mvc._

class Application extends Controller {

  /**
    * Default action for index.
    */
  def index = Action {
    Ok(views.html.index())
  }



}
