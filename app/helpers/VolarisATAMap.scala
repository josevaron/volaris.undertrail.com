package helpers

import com.undertrail.scrapers.helpers.{IataMap, AirportInfo}

/**
  * Created by Maicol_Smith on 22/12/2015.
  */
class VolarisIATAMap extends IataMap {

  val iataMap: Map[String, String] = Map(
  )

  val baseAirInfo = AirportInfo(handHeight = 40, handWidth = 40, handLength = 25, handWeight = 10, holdWeight = Some(25), extraBagPrice = None)

  val tzMap: Map[String, AirportInfo] = Map(
    "GUA" -> baseAirInfo.copy(tz = -6),
    "ACA" -> baseAirInfo.copy(tz = -6),
    "AGU" -> baseAirInfo.copy(tz = -6),
    "HUX" -> baseAirInfo.copy(tz = -6),
    "CME" -> baseAirInfo.copy(tz = -6),
    "CUL" -> baseAirInfo.copy(tz = -7),
    "CTM" -> baseAirInfo.copy(tz = -6),
    "CEN" -> baseAirInfo.copy(tz = -7),
    "CPE" -> baseAirInfo.copy(tz = -6),
    "CJS" -> baseAirInfo.copy(tz = -7),
    "CUU" -> baseAirInfo.copy(tz = -7),
    "CZM" -> baseAirInfo.copy(tz = -6),
    "GDL" -> baseAirInfo.copy(tz = -6),
    "HMO" -> baseAirInfo.copy(tz = -7),
    "BJX" -> baseAirInfo.copy(tz = -6),
    "LAP" -> baseAirInfo.copy(tz = -7),
    "MID" -> baseAirInfo.copy(tz = -6),
    "MTT" -> baseAirInfo.copy(tz = -6),
    "MEX" -> baseAirInfo.copy(tz = -6),
    "MTY" -> baseAirInfo.copy(tz = -6),
    "MZT" -> baseAirInfo.copy(tz = -7),
    "OAX" -> baseAirInfo.copy(tz = -6),
    "PVR" -> baseAirInfo.copy(tz = -6),
    "PXM" -> baseAirInfo.copy(tz = -6),
    "REX" -> baseAirInfo.copy(tz = -6),
    "SJD" -> baseAirInfo.copy(tz = -7),
    "SLP" -> baseAirInfo.copy(tz = -6),
    "TRC" -> baseAirInfo.copy(tz = -6),
    "TGZ" -> baseAirInfo.copy(tz = -6),
    "TIJ" -> baseAirInfo.copy(tz = -8),
    "TAM" -> baseAirInfo.copy(tz = -6),
    "TLC" -> baseAirInfo.copy(tz = -6),
    "CUN" -> baseAirInfo.copy(tz = -6),
    "VSA" -> baseAirInfo.copy(tz = -6),
    "VER" -> baseAirInfo.copy(tz = -6),
    "ZCL" -> baseAirInfo.copy(tz = -6),
    "ZIH" -> baseAirInfo.copy(tz = -6),
    "SJO" -> baseAirInfo.copy(tz = -6),
    "HAV" -> baseAirInfo.copy(tz = -5),
    "BOG" -> baseAirInfo.copy(tz = -5),
    "IAH" -> baseAirInfo.copy(tz = -6),
    "MIA" -> baseAirInfo.copy(tz = -5),
    "SAT" -> baseAirInfo.copy(tz = -6),
    "JFK" -> baseAirInfo.copy(tz = -5),
    "LAS" -> baseAirInfo.copy(tz = -8),
    "LAX" -> baseAirInfo.copy(tz = -8),
    "DFW" -> baseAirInfo.copy(tz = -6),
    "TKI" -> baseAirInfo.copy(tz = -6),
    "VRA" -> baseAirInfo.copy(tz = -5),
    "LIM" -> baseAirInfo.copy(tz = -5)

  )

  def getIATA(name: String): String = {
      iataMap.getOrElse(name,name)
  }

  def getTz(iata: String): Double = {
    tzMap.getOrElse(iata, baseAirInfo).tz
  }

  def getIataInfo(iata: String): Option[AirportInfo] = {
    tzMap.get(iata)
  }

  def getMoreRestrictiveInfo(origin: String, destination: String): AirportInfo = {
    val ai1 = tzMap.getOrElse(origin, baseAirInfo)
    val ai2 = tzMap.getOrElse(destination, baseAirInfo)

    (ai1.extraBagPrice, ai2.extraBagPrice) match {
      case (Some(x), Some(y)) if x>y => ai1
      case (Some(x), Some(y)) if x<=y => ai2
      case (_ ,_) => ai1 //If some of then doesn't support extra baggage, we don't support it
    }
  }

}

