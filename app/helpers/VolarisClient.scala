package helpers

import java.text.SimpleDateFormat
import java.time.{LocalDateTime, LocalDate}
import java.time.format.DateTimeFormatter
import java.util.{TimeZone, Calendar, Date}

import com.google.inject.{Inject, Singleton}
import com.undertrail.models._
import com.undertrail.scrapers.helpers._
import play.api.Configuration
import play.api.libs.ws.WSClient
import play.api.Logger

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent.Future
import scala.xml.{NodeSeq, Node}


@Singleton
class VolarisClient @Inject()(wSClient: WSClient, configuration: Configuration, iataMap: IataMap) extends ScraperClient {

  //---------------------------------------------------------------------
  // Definitions
  //---------------------------------------------------------------------

  val volarisFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
  val respFormatter: SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
  respFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))

  val QUERY_URL = configuration.getString("volaris.queryUrl").get
  val LOGON_URL = configuration.getString("volaris.sessionUrl").get

  val userName = configuration.getString("volaris.user").get
  val password = configuration.getString("volaris.password").get
  val accountNumber = configuration.getString("volaris.accountNumber").get
  val environment = configuration.getString("environment").get


  //---------------------------------------------------------------------
  // Functions
  //---------------------------------------------------------------------


  /**
   * This method request the corresponding page using the REQUEST_URL and returns
   * the list of flights obtained
    *
    * @param params object with all the params given in the request
   * @return list of Flight objects with the information received from the page
   */
  def getTickets(params: PSearch): Future[List[GTrip]] = {
    //We validate if the cookie is 1 hour old
      getSignature().flatMap { sign =>
      tripsWithXmlInfo(params, sign).map(_.map(_._1))
    }
  }

  def tripsWithXmlInfo(params: PSearch, sign: String): Future[List[(GTrip, List[XmlSegment])]] = {

      val searchXml = getSearchXML(params, sign)
      val httpHeaders: Seq[(String, String)] = Seq(
        "Content-Type" -> "text/xml;charset=UTF-8",
        "Accept-Encoding" -> "gzip,deflate",
        "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/GetAvailability"
      )

      wSClient.url(QUERY_URL)
        .withHeaders(httpHeaders: _*)
        .post(searchXml)
        .flatMap { response =>
          val resXml = response.xml
          validateErrors(resXml)
          val trips = (resXml \\ "Journey").map{ journey =>
            val segments = (journey \\ "Segment").map{ leg =>
              XmlSegment.create(leg)
            }

            val outLegs = segments.map{ xmlSegment =>

              val dTime = respFormatter.parse(xmlSegment.std)
              val dTimeUtc = fromutc(dTime, xmlSegment.departureStation)
              val aTime = respFormatter.parse(xmlSegment.sta)
              val aTimeUtc = fromutc(aTime, xmlSegment.arrivalStation)

              GLeg(FLIGHT, xmlSegment.departureStation, xmlSegment.arrivalStation, dTimeUtc.getTime, dTime.getTime,
                aTimeUtc.getTime, aTime.getTime, xmlSegment.flightDesignator.carrierCode, Some(xmlSegment.flightDesignator.flightNumber),None)
            }

            getPricing(params, segments.toList, sign).map{ pricing =>
              val bagLimits = BagLimits(20,30,40, 10, Some(25))

              val theTrip = GTrip.create(GRoute.fromLegList(outLegs.toList), None, pricing, Passengers(params.adults, params.children, params.infants),
                bagLimits, params, "volaris", Some((journey \\ "FareSellKey").text), System.currentTimeMillis(), System.currentTimeMillis()
              )
              (theTrip, segments.toList)
            }
          }
          Future.sequence(trips.toList)
        }

  }

  def getPricing(search: PSearch, segments: List[XmlSegment], signature: String): Future[Pricing] = {
    val searchXml = getPriceXML(search, segments, signature)
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/GetItineraryPrice"
    )

    wSClient.url(QUERY_URL)
      .withHeaders(httpHeaders: _*)
      .post(searchXml)
      .map { response =>
        val resXml = response.xml
              validateErrors(resXml)
              val currency = ((resXml \\ "Booking") \ "CurrencyCode").text
              val prices = scala.collection.mutable.Map[String,(Double, Double)]("ADT" -> (0.0, 0.0), "CHD" -> (0.0, 0.0))

              ( resXml \\ "PaxFare").foreach{ fare =>
                val (ntaxes, nbase, ndiscount) = (fare \\ "BookingServiceCharge").foldLeft((0.0,0.0,0.0)){ case ((ptax, pbase, pdiscount), node) =>
                  val (taxAdd, baseAdd, discountAdd) = (node \ "ChargeType").text match {
                    case "Tax" =>
                      (( node \ "Amount").text.toDouble , 0.0, 0.0)
                    case "FarePrice" =>
                      (0.0, ( node \ "Amount").text.toDouble, 0.0)
                    case "Discount" =>
                      (0.0, 0.0, ( node \ "Amount").text.toDouble)
                    case x =>
                      println("Unkown ChargeType "+x)
                      (0.0, ( node \ "Amount").text.toDouble, 0.0 )
                  }
            (ptax + taxAdd, pbase + baseAdd, pdiscount + discountAdd)
          }
          val paxType = (fare \ "PaxType").text
          prices(paxType) = (prices(paxType)._1 + ntaxes, prices(paxType)._2 + nbase - ndiscount)
        }

        val base = prices("ADT")._2 * search.adults + prices("CHD")._2 * search.children
        val taxes = prices("ADT")._1 * search.adults + prices("CHD")._1 * search.children
        Pricing(currency, base, 0, taxes, base+taxes, None)
      }
  }

  def getSignature(): Future[String] = {
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/ISessionManager/Logon"
    )

    val req = getLogonXML()
    wSClient.url(LOGON_URL)
      .withHeaders(httpHeaders: _*)
        .post(req.toString())
      .map { response =>
        (response.xml \\ "Signature").text
      }
  }

  def validateErrors(xmlDoc: Node) = {
    val error: NodeSeq = (xmlDoc \\ "faultstring")
    if(!error.isEmpty){
      println(xmlDoc)
      throw new Exception(error.text)
    }
  }

  def fromutc(date: Date, city: String): Date ={
    val cal_date = Calendar.getInstance()
    cal_date.setTime(date)
    cal_date.add(Calendar.HOUR, iataMap.getTz(city).toInt)

    cal_date.getTime
  }

  def getLogonXML() ={
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:ses="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService" xmlns:ses1="http://schemas.navitaire.com/WebServices/DataContracts/Session">
      <soapenv:Header>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <ses:LogonRequest>
          <ses:logonRequestData>
            <ses1:DomainCode>WWW</ses1:DomainCode>
            <ses1:AgentName>{userName}</ses1:AgentName>
            <ses1:Password>{password}</ses1:Password>
          </ses:logonRequestData>
        </ses:LogonRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def getSearchXML(search: PSearch, signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:enum="http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:GetAvailabilityRequest>
          <book1:TripAvailabilityRequest>
            <book1:AvailabilityRequests>
              <book1:AvailabilityRequest>
                <book1:DepartureStation>{search.origin}</book1:DepartureStation>
                <book1:ArrivalStation>{search.destination}</book1:ArrivalStation>
                <book1:BeginDate>{volarisFormatter.format(search.departureDate)}</book1:BeginDate>
                <book1:EndDate>{volarisFormatter.format(search.departureDate)}</book1:EndDate>
                <book1:FlightType>All</book1:FlightType>
                <book1:Dow>Daily</book1:Dow>
                <book1:CurrencyCode>MXN</book1:CurrencyCode>
                <book1:AvailabilityType>Default</book1:AvailabilityType>
                <book1:AvailabilityFilter>ExcludeUnavailable</book1:AvailabilityFilter>
                <book1:MaximumConnectingFlights>3</book1:MaximumConnectingFlights>
                <book1:PaxPriceTypes>
                  <book1:PaxPriceType>
                    {
                    for( i <- 0 until search.adults){
                      <book1:PaxType>ADT</book1:PaxType>
                    }
                    }
                    {
                    for( i <- 0 until search.children){
                      <book1:PaxType>CHD</book1:PaxType>
                    }
                    }
                    {
                    for( i <- 0 until search.infants){
                      <book1:PaxType>INF</book1:PaxType>
                    }
                    }

                  </book1:PaxPriceType>
                </book1:PaxPriceTypes>
              </book1:AvailabilityRequest>
            </book1:AvailabilityRequests>
          </book1:TripAvailabilityRequest>
        </book:GetAvailabilityRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

 def getPriceXML(search: PSearch, segments: List[XmlSegment],signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:PriceItineraryRequest>
          <book:ItineraryPriceRequest>
            <book1:PriceItineraryBy>JourneyWithLegs</book1:PriceItineraryBy>
            <book1:PriceJourneyWithLegsRequest>
              <book1:PriceJourneys>
                <book1:PriceJourney>
                  <com:State>New</com:State>
                  <book1:Segments>
                    {
                    segments.map{ segment =>
                      <book1:PriceSegment>
                        <com:State>New</com:State>
                        <book1:ActionStatusCode>NN</book1:ActionStatusCode>
                        <book1:ArrivalStation>{segment.arrivalStation}</book1:ArrivalStation>
                        <book1:DepartureStation>{segment.departureStation}</book1:DepartureStation>
                        <book1:STA>{segment.sta}</book1:STA>
                        <book1:STD>{segment.std}</book1:STD>
                        <book1:FlightDesignator>
                          <com:CarrierCode>{segment.flightDesignator.carrierCode}</com:CarrierCode>
                          <com:FlightNumber>{segment.flightDesignator.flightNumber}</com:FlightNumber>
                        </book1:FlightDesignator>
                        <book1:Fare>
                          <com:State>New</com:State>
                          <book1:ClassOfService>{segment.fare.classOfService}</book1:ClassOfService>
                          <book1:CarrierCode>{segment.fare.carrierCode}</book1:CarrierCode>
                          <book1:FareSequence>{segment.fare.fareSequence}</book1:FareSequence>
                          <book1:IsAllotmentMarketFare>{segment.fare.isAllotmentMarketFare}</book1:IsAllotmentMarketFare>
                          <book1:FareApplicationType>{segment.fare.fareApplicationType}</book1:FareApplicationType>
                        </book1:Fare>
                        <book1:PriceLegs>
                          <book1:PriceLeg>
                            <com:State>New</com:State>
                            <book1:ArrivalStation>{segment.arrivalStation}</book1:ArrivalStation>
                            <book1:DepartureStation>{segment.departureStation}</book1:DepartureStation>
                            <book1:STA>{segment.sta}</book1:STA>
                            <book1:STD>{segment.std}</book1:STD>
                            <book1:FlightDesignator>
                              <com:CarrierCode>{segment.flightDesignator.carrierCode}</com:CarrierCode>
                              <com:FlightNumber>{segment.flightDesignator.flightNumber}</com:FlightNumber>
                            </book1:FlightDesignator>
                          </book1:PriceLeg>
                        </book1:PriceLegs>
                      </book1:PriceSegment>
                    }
                    }
                  </book1:Segments>
                </book1:PriceJourney>
              </book1:PriceJourneys>
              <book1:PaxCount>{search.adults+search.children}</book1:PaxCount>
              <book1:CurrencyCode>MXN</book1:CurrencyCode>
              <book1:Passengers>
                {
                val codelist = List.range(0,search.adults).map{ x=>"ADT"} ++
                  List.range(0,search.children).map{x=>"CHD"}

                codelist.zipWithIndex.map{ case (code, i) =>
                  <book1:Passenger>
                    <com:State>New</com:State>
                    <book1:PassengerTypeInfos>
                      <book1:PassengerTypeInfo>
                        <com:State>New</com:State>
                        <book1:DOB>9999-12-31T00:00:00Z</book1:DOB>
                        <book1:PaxType>{code}</book1:PaxType>
                      </book1:PassengerTypeInfo>
                    </book1:PassengerTypeInfos>
                    {
                    if (i<search.infants){
                      <book1:PassengerInfants>
                        <book1:PassengerInfant>
                          <com:State>New</com:State>
                          <book1:DOB>9999-12-31T00:00:00Z</book1:DOB>
                        </book1:PassengerInfant>
                      </book1:PassengerInfants>
                    }
                    }
                  </book1:Passenger>
                }
                }

              </book1:Passengers>
              <book1:LoyaltyFilter>MonetaryOnly</book1:LoyaltyFilter>
            </book1:PriceJourneyWithLegsRequest>
          </book:ItineraryPriceRequest>
        </book:PriceItineraryRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  //------------------------------------------------------------------------------------------------------
  // Booking methods
  //------------------------------------------------------------------------------------------------------

  override def bookTrip(search: PSearch, uniqueId: String, passengers: List[BookingPassenger], contactInfo: ContactInfo): Future[BookingResponse] = {
        Logger.info("=================New Booking Request========================================")
        Logger.info(s"Current Time: ${LocalDateTime.now()}")
        Logger.info(s"Unique Id: $uniqueId")
        Logger.info(s"Search: $search")
        Logger.info(s"Passengers: ")
        passengers.foreach{x=>Logger.info(x.toString)}

        environment match {
          case "production" | "local" =>
            for ( sign <- getSignature();
                  fullTripInfo <- getTripInfo(search, uniqueId, sign);
                  theTrip <- sellRequest(fullTripInfo._1, fullTripInfo._2, passengers, sign);
                  theTrip <- sellInfantsRequest(theTrip, fullTripInfo._2, passengers, sign);
                  resPayment <- addPaymentRequest(theTrip, sign);
                  (pnr, balanceDue) <- commitRequest(theTrip, passengers, contactInfo, sign);
                  (newPnr, newBalanceDue) <- payBalanceDue(theTrip, pnr, balanceDue, passengers, contactInfo, sign)
            )
              yield {
                newBalanceDue match {
                  case 0 => BookingResponse(ALL_WERE_BOUGHT,"Ok", uniqueId,Some("volaris"), Some(pnr), Some(theTrip.price))
                  case x if x > 0 => BookingResponse(BOUGHT_WITH_BALANCE_DUE,s"The reservation was made with pnr $pnr but there is a balance due of $newBalanceDue.", uniqueId,Some("volaris"), None, None)
                  case x if x < 0 => BookingResponse(ALL_WERE_BOUGHT,s"The reservation was made with pnr $pnr but there is an extra balance of $newBalanceDue.", uniqueId,Some("volaris"), Some(pnr), Some(theTrip.price))
                }

              }
      case _ =>
        super.bookTrip(search, uniqueId, passengers, contactInfo)
    }
  }

  def sellRequest(trip: GTrip, segments: List[XmlSegment], passengers: List[BookingPassenger], signature: String): Future[GTrip] = {
    val sellXml = getSellXml(trip, segments, passengers, signature)
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/Sell"
    )

    wSClient.url(QUERY_URL)
      .withHeaders(httpHeaders: _*)
      .post(sellXml)
      .map { response =>
        val resXml = response.xml
        validateErrors(resXml)
        val finalPrice = (resXml \\ "TotalCost").text.toDouble
        val newBase = finalPrice - trip.price.fees - trip.price.taxes
        val newPricing = Pricing(trip.price.currency, newBase, trip.price.fees, trip.price.taxes, finalPrice, trip.price.bags)
        trip.copy(price = newPricing)
      }
  }

  def sellInfantsRequest(trip: GTrip, segments: List[XmlSegment], passengers: List[BookingPassenger], signature: String): Future[GTrip] = {
    passengers.filter(_.ageGroup()==INFANT).length match {
      case 0 => Future.successful(trip)
      case x =>
        val sellXml = getSellXmlInfants(trip, segments, passengers, signature)
        val httpHeaders: Seq[(String, String)] = Seq(
          "Content-Type" -> "text/xml;charset=UTF-8",
          "Accept-Encoding" -> "gzip,deflate",
          "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/Sell"
        )

        wSClient.url(QUERY_URL)
          .withHeaders(httpHeaders: _*)
          .post(sellXml)
          .map { response =>
            val resXml = response.xml
            val finalPrice = (resXml \\ "TotalCost").text.toDouble
            val newBase = finalPrice - trip.price.fees - trip.price.taxes
            val newPricing = Pricing(trip.price.currency, newBase, trip.price.fees, trip.price.taxes, finalPrice, trip.price.bags)
            trip.copy(price = newPricing)
          }
    }
  }

  def addPaymentRequest(trip: GTrip, signature: String): Future[String] = {
    Logger.info(s"Final Price: ${trip.price.total} ${trip.price.currency}")
    val payXml = paymentXml(trip, signature)
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/AddPaymentToBooking"
    )

    wSClient.url(QUERY_URL)
      .withHeaders(httpHeaders: _*)
      .post(payXml)
      .map { response =>
        val resXml = response.xml
        validateErrors(resXml)
        resXml.toString()
      }
  }

  def commitRequest(trip: GTrip, passengers: List[BookingPassenger], contactInfo: ContactInfo, signature: String, pnrOpt: Option[String] = None): Future[(String, Double)] = {
    val comXml = commitXml(trip, passengers, contactInfo, pnrOpt, signature)
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/IBookingManager/Commit"
    )

    wSClient.url(QUERY_URL)
      .withHeaders(httpHeaders: _*)
      .post(comXml)
      .map { response =>
        val resXml = response.xml
        validateErrors(resXml)
        val pnr = (resXml \\ "RecordLocator").text
        val balanceDue = (resXml \\ "BalanceDue").text.toDouble
        Logger.info(s"Pnr: $pnr")
        Logger.info(s"Balance Due: $balanceDue")
        (pnr, balanceDue)
      }
  }

  def payBalanceDue(trip: GTrip, pnr: String, balanceDue: Double, passengers: List[BookingPassenger], contactInfo: ContactInfo, signature: String): Future[(String, Double)] = {
    balanceDue match {
      case x if x <= 0 => Future.successful(("",balanceDue))
      case x if x >0 =>
        val newPrice = trip.price.copy(base = balanceDue, fees = 0, taxes = 0)
        val newTrip = trip.copy(price = newPrice)
        addPaymentRequest(newTrip, signature).flatMap{  resPayment =>
          commitRequest(trip, passengers, contactInfo, signature, Some(pnr))
        }
    }
  }

  def logout(signature: String): Future[String] = {
    val comXml = logoutXml(signature)
    val httpHeaders: Seq[(String, String)] = Seq(
      "Content-Type" -> "text/xml;charset=UTF-8",
      "Accept-Encoding" -> "gzip,deflate",
      "SOAPAction" -> "http://schemas.navitaire.com/WebServices/ISessionManager/Logout"
    )

    wSClient.url(QUERY_URL)
      .withHeaders(httpHeaders: _*)
      .post(comXml)
      .map { response =>
        val resXml = response.xml
        validateErrors(resXml)
        resXml.toString()
      }
  }

  def getTripInfo(search: PSearch, uniqueId: String, signature: String): Future[(GTrip,List[XmlSegment])] = {
    tripsWithXmlInfo(search, signature).map{ tripsInfo =>
      tripsInfo.find(_._1.getUniqueId == uniqueId) match {
        case Some(fullInfo) => fullInfo
        case None =>
          throw new Exception("Trip not found. Phase 1")
      }
    }
  }

  def getSellXml(trip: GTrip, segments: List[XmlSegment], passengers: List[BookingPassenger], signature: String) ={

    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:SellRequest>
          <book:SellRequestData>
            <book1:SellBy>Journey</book1:SellBy>
            <book1:SellJourneyRequest>
              <book1:SellJourneyRequestData>
                <book1:Journeys>
                  <book1:SellJourney>
                    <com:State>New</com:State>
                    <book1:NotForGeneralUse>false</book1:NotForGeneralUse>
                    <book1:Segments>
                      {
                      segments.map { segment =>
                        <book1:SellSegment>
                          <com:State>New</com:State>
                          <book1:ActionStatusCode>NN</book1:ActionStatusCode>
                          <book1:ArrivalStation>{segment.arrivalStation}</book1:ArrivalStation>
                          <book1:DepartureStation>{segment.departureStation}</book1:DepartureStation>
                          <book1:STA>{segment.sta}</book1:STA>
                          <book1:STD>{segment.std}</book1:STD>
                          <book1:FlightDesignator>
                            <com:CarrierCode>{segment.flightDesignator.carrierCode}</com:CarrierCode>
                            <com:FlightNumber>{segment.flightDesignator.flightNumber}</com:FlightNumber>
                          </book1:FlightDesignator>
                          <book1:Fare>
                            <com:State>New</com:State>
                            <book1:ClassOfService>{segment.fare.classOfService}</book1:ClassOfService>
                            <book1:CarrierCode>{segment.fare.carrierCode}</book1:CarrierCode>
                            <book1:FareSequence>{segment.fare.fareSequence}</book1:FareSequence>
                            <book1:IsAllotmentMarketFare>{segment.fare.isAllotmentMarketFare}</book1:IsAllotmentMarketFare>
                            <book1:FareApplicationType>{segment.fare.fareApplicationType}</book1:FareApplicationType>
                          </book1:Fare>
                        </book1:SellSegment>
                      }
                      }
                    </book1:Segments>
                  </book1:SellJourney>
                </book1:Journeys>
                <book1:PaxCount>{trip.search.adults + trip.search.children}</book1:PaxCount>
                <book1:CurrencyCode>MXN</book1:CurrencyCode>
                <book1:TypeOfSale>
                  <com:State>New</com:State>
                  <book1:PaxResidentCountry>MX</book1:PaxResidentCountry>
                </book1:TypeOfSale>
                <book1:Passengers>
                  {
                  val infants: List[Option[BookingPassenger]] = passengers.filter(_.ageGroup()==INFANT).map{ x=>Some(x)} ++ List.fill[Option[BookingPassenger]](passengers.length)(None)

                    passengers.filter(_.ageGroup()!=INFANT).zip(infants).zipWithIndex.map{ case ((passenger, infantOp), i) =>
                      <book1:Passenger>
                        <com:State>New</com:State>
                        <book1:PassengerNumber>{i}</book1:PassengerNumber>
                        <book1:FamilyNumber>0</book1:FamilyNumber>
                        <book1:Names>
                          <book1:BookingName>
                            <com:State>New</com:State>
                            <book1:FirstName>{passenger.firstName}</book1:FirstName>
                            <book1:LastName>{passenger.lastName}</book1:LastName>
                            <book1:Title>{passenger.title}</book1:Title>
                          </book1:BookingName>
                        </book1:Names>
                        <book1:PassengerAddresses>
                          <book1:PassengerAddress>
                            <com:State>New</com:State>
                            <book1:TypeCode>E</book1:TypeCode>
                            <book1:CompanyName>{s"${passenger.firstName} ${passenger.lastName}"}</book1:CompanyName>
                            <book1:Phone>{passenger.phoneNumber.countryCode+passenger.phoneNumber.areaCode+passenger.phoneNumber.number}</book1:Phone>
                          </book1:PassengerAddress>
                        </book1:PassengerAddresses>
                        <book1:PassengerTypeInfos>
                          <book1:PassengerTypeInfo>
                            <book1:State>New</book1:State>
                            <book1:DOB>{s"${passenger.birthday.toString}T00:00:00"}</book1:DOB>
                            <book1:PaxType>{getPassengerType(passenger)}</book1:PaxType>
                          </book1:PassengerTypeInfo>
                        </book1:PassengerTypeInfos>
                        <book1:PassengerInfos>
                          <book1:PassengerInfo>
                            <com:State>New</com:State>
                            <book1:BalanceDue>0</book1:BalanceDue>
                            <book1:Gender>{passenger.gender match { case "MALE" => "Male" case "FEMALE" => "Female"}}</book1:Gender>
                            <book1:TotalCost>0</book1:TotalCost>
                            <book1:WeightCategory>{passenger.gender match { case "MALE" => "Male" case "FEMALE" => "Female"}}</book1:WeightCategory>
                          </book1:PassengerInfo>
                        </book1:PassengerInfos>
                        {
                          infantOp match { case Some(infant) =>
                        <book1:Infant>
                          <book1:PassengerInfant>
                            <com:State>New</com:State>
                            <book1:DOB>{s"${infant.birthday}T00:00:00"}</book1:DOB>
                            <book1:Gender>{infant.gender match { case "M" => "Male" case "F" => "Female" }}</book1:Gender>
                            <book1:Nationality>{infant.nationality}</book1:Nationality>
                            <book1:ResidentCountry>MX</book1:ResidentCountry>
                            <book1:Names>
                              <book1:BookingName>
                                <com:State>New</com:State>
                                <book1:FirstName>{infant.firstName}</book1:FirstName>
                                <book1:LastName>{infant.lastName}</book1:LastName>
                                <book1:Title>CHILD</book1:Title>
                              </book1:BookingName>
                            </book1:Names>
                          </book1:PassengerInfant>
                        </book1:Infant>
                          case None =>
                      }}
                          </book1:Passenger>

                        }
                  }
                </book1:Passengers>
              </book1:SellJourneyRequestData>
            </book1:SellJourneyRequest>
          </book:SellRequestData>
        </book:SellRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def getSellXmlInfants(trip: GTrip, segments: List[XmlSegment], passengers: List[BookingPassenger], signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:SellRequest>
          <book:SellRequestData>
            <book1:SellBy>SSR</book1:SellBy>
            <book1:SellSSR>
              <book1:SSRRequest>
                <book1:SegmentSSRRequests>
                  {segments.map { segment =>
                  <book1:SegmentSSRRequest>
                    <book1:FlightDesignator>
                      <com:CarrierCode>{segment.flightDesignator.carrierCode}</com:CarrierCode>
                      <com:FlightNumber>{segment.flightDesignator.flightNumber}</com:FlightNumber>
                    </book1:FlightDesignator>
                    <book1:STD>{segment.std}</book1:STD>
                    <book1:DepartureStation>{segment.departureStation}</book1:DepartureStation>
                    <book1:ArrivalStation>{segment.arrivalStation}</book1:ArrivalStation>
                    <book1:PaxSSRs>
                      {
                        passengers.filter(_.ageGroup()==INFANT).zipWithIndex.map{ case (inf, indx) =>
                          <book1:PaxSSR>
                            <com:State>New</com:State>
                            <book1:ActionStatusCode>NN</book1:ActionStatusCode>
                            <book1:ArrivalStation>{segment.arrivalStation}</book1:ArrivalStation>
                            <book1:DepartureStation>{segment.departureStation}</book1:DepartureStation>
                            <book1:PassengerNumber>{indx}</book1:PassengerNumber>
                            <book1:SSRCode>INF</book1:SSRCode>
                            <book1:SSRNumber>0</book1:SSRNumber>
                            <book1:SSRValue>0</book1:SSRValue>
                          </book1:PaxSSR>
                        }
                      }
                    </book1:PaxSSRs>
                  </book1:SegmentSSRRequest>
                }}
                </book1:SegmentSSRRequests>
                <book1:CurrencyCode>MXN</book1:CurrencyCode>
                <book1:CancelFirstSSR>false</book1:CancelFirstSSR>
                <book1:SSRFeeForceWaiveOnSell>false</book1:SSRFeeForceWaiveOnSell>
              </book1:SSRRequest>
            </book1:SellSSR>
            <book1:SellFee>
              <book1:SellFeeRequestData>
                <book1:PassengerNumber>0</book1:PassengerNumber>
                <book1:SellFeeType>ServiceFee</book1:SellFeeType>
              </book1:SellFeeRequestData>
            </book1:SellFee>
          </book:SellRequestData>
        </book:SellRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def paymentXml(trip: GTrip, signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:AddPaymentToBookingRequest>
          <book1:addPaymentToBookingReqData>
            <book1:MessageState>New</book1:MessageState>
            <book1:WaiveFee>false</book1:WaiveFee>
            <book1:ReferenceType>Default</book1:ReferenceType>
            <book1:PaymentMethodType>AgencyAccount</book1:PaymentMethodType>
            <book1:PaymentMethodCode>AG</book1:PaymentMethodCode>
            <book1:QuotedCurrencyCode>{trip.price.currency}</book1:QuotedCurrencyCode>
            <book1:QuotedAmount>{round(trip.price.total)}</book1:QuotedAmount>
            <book1:Status>New</book1:Status>
            <book1:AccountNumberID>0</book1:AccountNumberID>
            <book1:AccountNumber>{accountNumber}</book1:AccountNumber>
            <book1:Expiration>0001-01-01T00:00:00</book1:Expiration>
            <book1:ParentPaymentID>0</book1:ParentPaymentID>
            <book1:Installments>0</book1:Installments>
            <book1:PaymentText></book1:PaymentText>
            <book1:Deposit>0</book1:Deposit>
            <book1:AuthorizationCode>0</book1:AuthorizationCode>
          </book1:addPaymentToBookingReqData>
        </book:AddPaymentToBookingRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def commitXml(trip: GTrip, passengers: List[BookingPassenger], contact: ContactInfo, pnrOpt: Option[String], signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:book="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService" xmlns:book1="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:com="http://schemas.navitaire.com/WebServices/DataContracts/Common" xmlns:arr="http://schemas.microsoft.com/2003/10/Serialization/Arrays" xmlns:sys="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <book:CommitRequest>
          <book1:BookingRequest>
            <book1:Booking>
              <book1:State>New</book1:State>
              {
                pnrOpt match {
                  case Some(pnr) => <book1:RecordLocator>{pnr}</book1:RecordLocator>
                  case None =>
                }
              }
              <book1:CurrencyCode>MXN</book1:CurrencyCode>
              <book1:PaxCount>{trip.search.adults + trip.search.children}</book1:PaxCount>
              <book1:BookingID>0</book1:BookingID>
              <book1:BookingParentID>0</book1:BookingParentID>
              <book1:Passengers>
                {
                   passengers.filter(_.ageGroup()!=INFANT).zipWithIndex.map { case (passenger, i) =>
                     <book1:Passenger>
                       <com:State>Clean</com:State>
                       <book1:PassengerNumber>{i}</book1:PassengerNumber>
                       <book1:FamilyNumber>0</book1:FamilyNumber>
                       <book1:Names>
                         <book1:BookingName>
                           <com:State>New</com:State>
                           <book1:FirstName>{passenger.firstName}</book1:FirstName>
                           <book1:LastName>{passenger.lastName}</book1:LastName>
                           <book1:Title>{passenger.title}</book1:Title>
                         </book1:BookingName>
                       </book1:Names>
                       <book1:PassengerInfo>
                         <com:State>New</com:State>
                         <book1:BalanceDue>0</book1:BalanceDue>
                         <book1:Gender>{passenger.gender match { case "MALE" => "Male" case "FEMALE" => "Female"}}</book1:Gender>
                         <book1:Nationality>{passenger.nationality}</book1:Nationality>
                         <book1:ResidentCountry>MX</book1:ResidentCountry>
                         <book1:TotalCost>0</book1:TotalCost>
                         <book1:WeightCategory>{passenger.gender match { case "MALE" => "Male" case "FEMALE" => "Female"}}</book1:WeightCategory>
                       </book1:PassengerInfo>
                       <book1:PassengerTypeInfos>
                         <book1:PassengerTypeInfo>
                           <book1:State>New</book1:State>
                           <book1:DOB>{s"${passenger.birthday.toString}T00:00:00"}</book1:DOB>
                           <book1:PaxType>{getPassengerType(passenger)}</book1:PaxType>
                         </book1:PassengerTypeInfo>
                       </book1:PassengerTypeInfos>

                       {
                       if (i == 0) {
                         <book1:PassengerInfants>
                           {passengers.filter(_.ageGroup() == INFANT).map { infant =>
                           <book1:PassengerInfant>
                             <com:State>New</com:State>
                             <book1:DOB>{s"${infant.birthday}T00:00:00"}</book1:DOB>
                             <book1:Gender>{infant.gender match {case "MALE" => "Male" case "FEMALE" => "Female" }}</book1:Gender>
                             <book1:Nationality>{infant.nationality}</book1:Nationality>
                             <book1:ResidentCountry>MX</book1:ResidentCountry>
                             <book1:Names>
                               <book1:BookingName>
                                 <com:State>New</com:State>
                                 <book1:FirstName>{infant.firstName}</book1:FirstName>
                                 <book1:LastName>{infant.lastName}</book1:LastName>
                                 <book1:Title>{infant.title}</book1:Title>
                               </book1:BookingName>
                             </book1:Names>
                           </book1:PassengerInfant>
                         }}

                         </book1:PassengerInfants>
                       }}

                     </book1:Passenger>
                   }
                }
              </book1:Passengers>
              <book1:BookingContacts>
                <book1:BookingContact>
                  <com:State>New</com:State>
                  <book1:TypeCode>P</book1:TypeCode>
                  <book1:Names>
                    <book1:BookingName>
                      <com:State>New</com:State>
                      <book1:FirstName>{contact.firstName}</book1:FirstName>
                      <book1:LastName>{contact.lastName}</book1:LastName>
                      <book1:Title>{contact.title}</book1:Title>
                    </book1:BookingName>
                  </book1:Names>
                  <book1:EmailAddress>{contact.email}</book1:EmailAddress>
                  <book1:HomePhone>{contact.phoneNumber.countryCode+contact.phoneNumber.areaCode+contact.phoneNumber.number}</book1:HomePhone>
                  <book1:AddressLine1>{contact.address}</book1:AddressLine1>
                  <book1:City>{contact.city}</book1:City>
                  <book1:ProvinceState>{contact.city.take(3)}</book1:ProvinceState>
                  <book1:PostalCode>{contact.postalCode}</book1:PostalCode>
                  <book1:CountryCode>{contact.country.take(2)}</book1:CountryCode>
                  <book1:DistributionOption>None</book1:DistributionOption>
                  <book1:NotificationPreference>None</book1:NotificationPreference>
                </book1:BookingContact>
              </book1:BookingContacts>
            </book1:Booking>


            <book1:RestrictionOverride>false</book1:RestrictionOverride>
            <book1:ChangeHoldDateTime>false</book1:ChangeHoldDateTime>
            <book1:WaiveNameChangeFee>false</book1:WaiveNameChangeFee>
            <book1:WaivePenaltyFee>false</book1:WaivePenaltyFee>
            <book1:WaiveSpoilageFee>false</book1:WaiveSpoilageFee>
            <book1:DistributeToContacts>true</book1:DistributeToContacts>
          </book1:BookingRequest>
        </book:CommitRequest>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def logoutXml(signature: String) = {
    <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://schemas.navitaire.com/WebServices" xmlns:ses="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService">
      <soapenv:Header>
        <web:Signature>{signature}</web:Signature>
        <web:ContractVersion>0</web:ContractVersion>
      </soapenv:Header>
      <soapenv:Body>
        <ses:LogoutRequest/>
      </soapenv:Body>
    </soapenv:Envelope>
  }

  def getPassengerType(passenger: BookingPassenger): String = {
    passenger.ageGroup() match {
      case ADULT => "ADT"
      case CHILDREN => "CHD"
      case INFANT => "INF"
    }
  }

  def round(x: Double): Double = {
    (x * 100).toLong.toDouble/100
  }


}

case class XmlFare (
                     classOfService: String,
                     carrierCode: String,
                     fareSequence: String,
                     isAllotmentMarketFare: String,
                     fareApplicationType: String
                   )

case class XmlFlightDesignator (
                                 carrierCode: String,
                                 flightNumber: String
                               )

case class XmlSegment (
                    arrivalStation: String,
                    departureStation: String,
                    sta: String,
                    std: String,
                    flightDesignator: XmlFlightDesignator,
                    fare: XmlFare
                  )
object XmlSegment {
  def create(element: Node): XmlSegment = {
    val origin = (element \ "DepartureStation").text
    val destination = (element \ "ArrivalStation").text
    val std = (element \ "STD").text
    val sta = (element \ "STA").text
    val operator = (element \ "FlightDesignator" \ "CarrierCode" ).text
    val tickNum = (element \ "FlightDesignator" \ "FlightNumber").text
    val fareNode = (element \ "Fares" \ "Fare").head
    val classOfService = (fareNode \ "ClassOfService").text
    val carrierCode = (fareNode \ "CarrierCode").text
    val fareSequence = (fareNode \ "FareSequence").text
    val isAllotmentMarketFare = (fareNode \ "IsAllotmentMarketFare").text
    val fareApplicationType = (fareNode \ "FareApplicationType").text

    XmlSegment(destination, origin, sta, std, XmlFlightDesignator(operator, tickNum),
      XmlFare(classOfService, carrierCode, fareSequence, isAllotmentMarketFare, fareApplicationType))

  }
}


